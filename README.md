## Demo ##

`npm install`

`node index.js` for english cv demo

`node vi_index.js` for vietnamese cv demo

I tried to do a general parser for different kind of csv, due to small size of sample given out, there's not much point in tweaking the parser or the learner. I can make it get every detail of these samples, but that's it.

Anyway you can't not use it anyway to parse your millions of records sitting at your storage. This is just a demo.

So, I'm waiting for an offer...

L.
