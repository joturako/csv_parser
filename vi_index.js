var fs = require("fs");
var WordExtractor = require("word-extractor");
var csv = require('fast-csv');
var request = require('request');
var chrono = require('chrono-node');
var extractor = new WordExtractor();
var collectedData = {
    addresses: [],
    educations: [],
    experiences: []
};
var dataFlag = {
    'language': 0, // 0 for en, 1 for vi
    'name': false,
    'address': false,
    'salutation': false,
    'gender': false,
    'dob': false,
    'nationality': false,
    'country': false,
    'phone': false,
    'email': false,
    'marital': false,

    'employment': false,
    'eudcation': false
};
var googleAPIKey_geocoding = 'AIzaSyDIrilcnsHyxw57nnCTvwy4gGSBQotZrD0';
var googleAPIURL = 'https://maps.googleapis.com/maps/api/geocode/json?key=' + googleAPIKey_geocoding + '&address=';
var limduParser = require('./limduParser.js');
var specialChars = RegExp("[\\t|\\b]", "g");

function normalizeAddress(input) {
    return input.replace(/street/i, 'st');
}

function startParse() {
    var doc = extractor.extract("sample_cv_vi.doc")
        .then(function(doc) {
            var body = doc.getBody();
            var body_resp = body.split("\r");
            for (var i = 0; i < body_resp.length; i++) {
                if (body_resp[i] != '') {
                    normalized = body_resp[i].replace(specialChars, ' ').trim();
                    parseLine(normalized);
                }
            }
            console.log(collectedData);
        });

};

function parseLine(line) {
    result = limduParser.classifier.classify(line);
    if (result.length == 1) {
        if (result == 'address') {
            parts = line.split(':');
            collectedData.addresses.push(parts);
        }
        if (result == 'dob') {
            dob = chrono.parseDate(line);
            if (dob) {
                collectedData.dob = dob;
            }
        }
        if (result == "name") {
            parts = line.split(':');
            collectedData.name = parts[parts.length - 1].trim();
        }
        if (result == "gender") {
            parts = line.split(':');
            collectedData.gender = parts[parts.length - 1].trim();
        }
        if (result == "marital") {
            parts = line.split(':');
            collectedData.marital = parts[parts.length - 1].trim();
        }
        if (result == "country") {
            parts = line.split(':');
            collectedData.country = parts[parts.length - 1].trim();
        }
        if (result == "phone") {
            parts = line.split(':');
            collectedData.phone = parts[parts.length - 1].trim();
        }
        if (result == "email") {
            var emailRegex = /([-a-z0-9~!$%^&*_=+}{\'?]+(\.[-a-z0-9~!$%^&*_=+}{\'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?)/;
            var matches = line.match(emailRegex);
            if (matches) {
                collectedData.email = matches[0];
            }
        }
        if (result == 'education') {
            //console.log(line);
        }
        if (result == 'degree') {
            if (collectedData.educations.length > 0) {
                collectedData.educations[collectedData.educations.length - 1].degree = line.trim();
            }
        }
    }
    if (result.length < 3) {
        if (result.indexOf('education') > -1) {
            var yearsPattern = /(19|20)\d{2}/g;
            var years = line.match(yearsPattern);
            if (years) {
                if (years.length == 2) { //time period
                    collectedData.educations.push({
                        'start': years[0],
                        'end': years[1],
                        'school': line.replace(years[0], '').replace(years[1], '').replace(/\s{2,}/g, '').trim()
                    });
                }
                if (years.length == 1) { //point in time

                }
            }
        }
    }
}

limduParser.init('vi', function(parser) {
    startParse();
});
