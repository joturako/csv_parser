var limdu = require('limdu');
var fs = require('fs');
var csv = require('fast-csv');
var exports = module.exports = {};
exports.classifier = null;
exports.init = function(lang, cb) {

    // First, define our base classifier type (a multi-label classifier based on winnow):
    // var TextClassifier = limdu.classifiers.multilabel.BinaryRelevance.bind(0, {
    //     binaryClassifierType: limdu.classifiers.Winnow.bind(0, {
    //         retrain_count: 10
    //     })
    // });

    var TextClassifier = limdu.classifiers.multilabel.PassiveAggressive.bind(0, {
        Constant: 5.0,
        retrain_count: 10,
    });
    // Now define our feature extractor - a function that takes a sample and adds features to a given features set:
    var WordExtractor = function(input, features) {

        input.split(" ").forEach(function(word) {

            features[word] = 1;
        });

    };

    // Initialize a classifier with the base classifier type and the feature extractor:
    var intentClassifier = new limdu.classifiers.EnhancedClassifier({
        classifierType: TextClassifier,
        normalizer: limdu.features.LowerCaseNormalizer,
        featureExtractor: lang == 'vi' ? limdu.features.NGramsOfWords(2) : limdu.features.NGramsOfWords(1)
    });

    var trainData = [];

    var stream = fs.createReadStream("train_data_" + lang + ".csv");

    var csvStream = csv
        .parse()
        .on("data", function(data) {
            trainData.push({
                input: data[1],
                output: data[0]
            });

        })
        .on("end", function() {

            // Train and test:
            intentClassifier.trainBatch(trainData);
            exports.classifier = intentClassifier;
            cb();

        });

    stream.pipe(csvStream);

};
